HOSTNAME=$1
KEY=~/.ssh/ch9_shared.pem 


ssh -o StrictHostKeyChecking=no -i $KEY ubuntu@$HOSTNAME '

# To update virtual repositories
sudo apt update -y

# Next check if Java is already installed:

if java -version ;
then 
    sudo apt install default-jre 
else:
   echo "Java already installed" 
fi

# To install add on Development Kit
sudo apt install default-jdk -y

wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -

sudo sh -c "echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list"

wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -

sudo apt update 

sudo apt install jenkins -y

sudo systemctl start jenkins

sudo cat /var/lib/jenkins/secrets/initialAdminPassword

'


